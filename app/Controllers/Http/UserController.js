'use strict';

const User = use('App/Models/User');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class UserController {

  async index() {
    return await User.query().with('tweets').fetch()
  }

  async show({params}) {
    return await User.findOrFail(params.id)
  }
}

module.exports = UserController;
