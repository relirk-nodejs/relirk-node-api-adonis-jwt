'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/', () => {
  return {
    "app": "relirk-node-api-adonis-jwt",
    "version": "4.1.0",
    "up": true,
    "dependencies": [
      { "name": "PostgreSQL", "up": true },
    ]
  }
});
Route.post('/register', 'AuthController.register');
Route.post('/authenticate', 'AuthController.authenticate');
Route.get('/test-auth', 'TestAuthController.index').middleware(['auth:jwt']);

Route.group(() => {
  Route.resource('tweets', 'TweetController').apiOnly().except('update');
  Route.resource('users', 'UserController').apiOnly().except(['update', 'destroy', 'store'])
}).middleware(['auth:jwt']);


